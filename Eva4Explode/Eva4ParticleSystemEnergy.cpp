#include "Eva4ParticleSystemEnergy.h"

bool Eva4ParticleSystemEnergy::initWithDuration(float duration){
    if (!CDParticleSystemProgress::initWithFileAndDuration(kExplodeEnergyPlistName, duration)) return false;
    this->pause();
    return true;
}

void Eva4ParticleSystemEnergy::updateParticle(tCCParticle *particle, float progress){
    static float width = CCDirector::sharedDirector()->getWinSize().width;
    
    static key_frame sizes[3] = {
        key_frame(0, width * 0),
        key_frame(0.1, width * .08),
        key_frame(1, width * .06)
    };
    static int sizeIndex = 0;
    float size = seekValueOfProgress(_progress, &sizeIndex, sizes, 3);
    this->setEndSize(size);
    
    static key_frame speeds[3] = {
        key_frame(0, width * 0),
        key_frame(0.05, width * .6),
        key_frame(1, width * .75)
    };
    static int speedIndex = 0;
    float speed = seekValueOfProgress(_progress, &speedIndex, speeds, 3);
    this->setSpeed(speed);
    
    static key_frame accelerations[3] = {
        key_frame(0, -width * 0),
        key_frame(0.05, -width * .7),
        key_frame(1, -width * .85)
    };
    static int accelerationIndex = 0;
    float acceleration = seekValueOfProgress(_progress, &accelerationIndex, accelerations, 3);
    this->setRadialAccel(acceleration);
}