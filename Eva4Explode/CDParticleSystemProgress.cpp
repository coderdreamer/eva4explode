#include "CDParticleSystemProgress.h"

#pragma mark - Life
bool CDParticleSystemProgress::initWithFileAndDuration(const char *plistFile, float duration){
    if (!CCParticleSystemQuad::initWithFile(plistFile)) return false;
    _progress = 0;
    _updateProgress = true;
    _duration = duration;
    _keyframeIndex = 0;
    return true;
}

bool CDParticleSystemProgress::initWithFile(const char *plistFile){
    if (!CCParticleSystemQuad::initWithFile(plistFile)) return false;
    _progress = 0;
    _updateProgress = true;
    _duration = this->getDuration();
    _keyframeIndex = 0;
    return true;
}

#pragma mark - Setter & Getter
void CDParticleSystemProgress::setUpdateProgress(bool update){
    _updateProgress = update;
}

void CDParticleSystemProgress::setProgress(float progress){
    if (progress < 0) progress = 0;
    if (progress > 1) progress = 1;
    _playbackTime = progress * _duration;
    _progress = progress;
}

float CDParticleSystemProgress::getProgress(){
    return _progress;
}

void CDParticleSystemProgress::timeIncrease(float increament){
    float newProgress = _progress + increament / _duration;
    this->setProgress(newProgress);
}

#pragma mark - Update
void CDParticleSystemProgress::update(float dt){
    if (_playbackTime < _duration && _updateProgress) {
        _playbackTime += dt;
        this->setProgress(_playbackTime / _duration);
    }
    
    CCParticleSystemQuad::update(dt);
}

void CDParticleSystemProgress::updateQuadWithParticle(tCCParticle* particle, const CCPoint& newPosition){
    this->updateParticle(particle, _progress);
    CCParticleSystemQuad::updateQuadWithParticle(particle, newPosition);
}

float seekValueOfProgress(float progress, int* index, key_frame* keyFrames, int framesCount){
    float value;
    while (1) {
        key_frame present = keyFrames[*index];
        float pProgress = present.progress;
        if (pProgress < progress) {
            key_frame next = keyFrames[*index + 1];
            if (next.progress > progress) {
                float scale = (progress - present.progress) / (next.progress - present.progress);
                value = scale * (next.value - present.value) + present.value;
                break;
            } else {
                (*index)++;
            }
        }else if (pProgress == progress){
            value = keyFrames[*index].value;
            break;
        }else if (pProgress > progress){
            (*index)--;
        }
    }
    return value;
}

#pragma mark - Control
void CDParticleSystemProgress::play(){
    this->setUpdateProgress(true);
}

void CDParticleSystemProgress::pause(){
    this->setUpdateProgress(false);
}