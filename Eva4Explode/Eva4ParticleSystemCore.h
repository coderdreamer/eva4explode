#define kExplodeCorePlistName "ExplodeCore.plist"

#ifndef __Eva4Explode__Eva4ParticalSystemCore__
#define __Eva4Explode__Eva4ParticalSystemCore__
#include "cocos2d.h"
#include "CDParticleSystemProgress.h"

class Eva4ParticleSystemCore : public CDParticleSystemProgress{
public:
    virtual bool initWithDuration(float duration);
private:
    virtual void updateParticle(tCCParticle* particle, float progress);
};

#endif /* defined(__Eva4Explode__Eva4ParticalSystemCore__) */
