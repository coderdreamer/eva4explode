
#ifndef __Eva4Explode__Eva4Explode__
#define __Eva4Explode__Eva4Explode__

#include "cocos2d.h"
#include "Eva4ParticleSystemCore.h"
#include "Eva4ParticleSystemEnergy.h"
#include "Eva4ParticleSystemCloud.h"
using namespace cocos2d;

typedef enum{
    Eva4ExplodeStatePlay,
    Eva4ExplodeStatePause
}Eva4ExplodeState;

class Eva4ExplodeDelegate;
class Eva4Explode : public cocos2d::CCLayer
{
    Eva4ExplodeState _state;
    float _duration;
    float _updateCheckFrequency;
    int _audioId;
    Eva4ParticleSystemCore* _explodeCore;
    Eva4ParticleSystemEnergy* _explodeEnergy;
    Eva4ParticleSystemCloud* _explodeCloud;
public:
    virtual bool init();
    CREATE_FUNC(Eva4Explode);
    
    /*Control*/
    void pause();
    void play();
    void pauseOrPlay();
    void timeIncrease(float increament);
    float getDuration();
    void setDelegate(Eva4ExplodeDelegate* delegate);
private:
    void configureExplodeCore();
    void checkUpdate();
    Eva4ExplodeDelegate* _delegate;
};

class Eva4ExplodeDelegate {
public:
    virtual void explodeUpdateProgress(Eva4Explode* explode, float progress){};
};

#endif /* defined(__Eva4Explode__Eva4Explode__) */
