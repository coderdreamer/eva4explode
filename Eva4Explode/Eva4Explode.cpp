#include "Eva4Explode.h"
#include "SimpleAudioEngine.h"
using namespace CocosDenshion;

bool Eva4Explode::init(){
    if (!CCLayer::init()) return false;
    
    _duration = 4.0f;
    _updateCheckFrequency = 0.02f;
    this->schedule(schedule_selector(Eva4Explode::checkUpdate), _updateCheckFrequency);
    _state = Eva4ExplodeStatePause;
    
    //Wherever after using SimpleAudioEngine relative functions, a EXC_BAD_ACCESS raise when enter background.
    //I think it's a bug of cocos2d 2.0
    SimpleAudioEngine::sharedEngine()->preloadEffect("nuclear.mp3");
    
    CCSize size = this->getContentSize();
    CCPoint center = ccp(size.width / 2, size.height / 2);
    
    _explodeCloud = new Eva4ParticleSystemCloud();
    _explodeCloud->initWithDuration(_duration);
    _explodeCloud->autorelease();
    this->addChild(_explodeCloud);
    _explodeCloud->setScaleY(0.4);
    _explodeCloud->setBlendFunc((ccBlendFunc){GL_ONE_MINUS_DST_COLOR, GL_ONE});
    //_explodeCloud->setBlendFunc((ccBlendFunc){GL_SRC_COLOR, GL_ONE});
    _explodeCloud->setPosition(center.x, size.height* 0.9);

    _explodeCore = new Eva4ParticleSystemCore();
    _explodeCore->initWithDuration(_duration);
    _explodeCore->autorelease();
    this->addChild(_explodeCore);
    _explodeCore->setPosition(center);

    _explodeEnergy = new Eva4ParticleSystemEnergy();
    _explodeEnergy->initWithDuration(_duration);
    _explodeEnergy->autorelease();
    this->addChild(_explodeEnergy);
    _explodeEnergy->setBlendFunc((ccBlendFunc){GL_DST_COLOR, GL_ONE_MINUS_CONSTANT_ALPHA});
    _explodeEnergy->setPosition(center);

    return true;
}

void Eva4Explode::configureExplodeCore(){
    CCParticleSystemQuad* particleSystem = _explodeCore;
    CCTexture2D* texture;
    particleSystem->setTexture(texture);
}

#pragma mark - Setter & Getter
float Eva4Explode::getDuration(){
    return _duration;
}

void Eva4Explode::setDelegate(Eva4ExplodeDelegate *delegate){
    _delegate = delegate;
}

#pragma mark - Control
void Eva4Explode::pause(){
    if (_state == Eva4ExplodeStatePause) return;
    _explodeCore->pause();
    _explodeEnergy->pause();
    _explodeCloud->pause();

    SimpleAudioEngine::sharedEngine()->pauseEffect(_audioId);
    
    _state = Eva4ExplodeStatePause;
}

void Eva4Explode::play(){
    if (_state == Eva4ExplodeStatePlay) return;
    _explodeCore->play();
    _explodeEnergy->play();
    _explodeCloud->play();
    
    if (_explodeCore->getProgress() == 0) {
        _audioId = SimpleAudioEngine::sharedEngine()->playEffect("nuclear.mp3");
    }else{
        SimpleAudioEngine::sharedEngine()->resumeEffect(_audioId);
    }
    
    _state = Eva4ExplodeStatePlay;
}

void Eva4Explode::pauseOrPlay(){
    switch (_state) {
        case Eva4ExplodeStatePause:
            this->play();
            break;
        case Eva4ExplodeStatePlay:
            this->pause();
            break;
        default:
            break;
    }
}

void Eva4Explode::timeIncrease(float increament){
    _explodeCore->timeIncrease(increament);
    _explodeEnergy->timeIncrease(increament);
    _explodeCloud->timeIncrease(increament);
}

#pragma mark - Update
void Eva4Explode::checkUpdate(){
    float progress = _explodeCore->getProgress();
    _delegate->explodeUpdateProgress(this, progress);
}