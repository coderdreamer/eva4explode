#include "Eva4ExplodeScene.h"

bool Eva4ExplodeScene::init(){
    if (!CCScene::init()) return false;
        
    CCSize size = this->getContentSize();
    CCPoint center = ccp(size.width / 2, size.height / 2);
    
    _sky = CCSprite::create("sky.png");
    _sky->setPosition(center);
    this->addChild(_sky);
    
    _explode = Eva4Explode::create();
    _explode->setPosition(ccp(0.0f, size.height * -0.43));
    _explode->setDelegate(this);
    this->addChild(_explode);
    
    _holder = CCHolder::create();
    this->addChild(_holder);
    _holder->setDelegate(this);
    
    _ground = CCSprite::create("ground.png");
    _ground->setPosition(center);
    this->addChild(_ground);
    
    return true;
}

#pragma mark - CCHolderDelegate
static bool pauseLocked = false;
void Eva4ExplodeScene::holderTouchedDown(CCHolder* holder){
    //printf("holderTouchedDown\n");
    if (!pauseLocked) _explode->pause();
}

void Eva4ExplodeScene::holderTouchedUp(CCHolder* holder){
    //printf("holderTouchedUp\n");
    if (!pauseLocked) _explode->play();
}

void Eva4ExplodeScene::holderSingleTaped(CCHolder* holder){
    //printf("holderSingleTaped\n");
}

void Eva4ExplodeScene::holderDoubleTaped(CCHolder* holder){
    //printf("holderDoubleTaped\n");
    _explode->pauseOrPlay();
    pauseLocked = pauseLocked ? false : true;
}

void Eva4ExplodeScene::holderStartHovering(CCHolder* holder){
    //printf("holderStartHovering\n");
}

void Eva4ExplodeScene::holderEndHovering(CCHolder* holder){
    //printf("holderEndHovering\n");
}

void Eva4ExplodeScene::holderMovingHorizontally(CCHolder *holder, float increament){
    //printf("holderMovingHorizontally:%f\n", increament);
    static float width = this->getContentSize().width;
    float time = increament / width * _explode->getDuration();
    _explode->timeIncrease(time);
}

#pragma mark - Eva4ExplodeDelegate
void Eva4ExplodeScene::explodeUpdateProgress(Eva4Explode *explode, float progress){
    static key_frame opacities[4] = {
        key_frame(0, 255),
        key_frame(0.15, 255),
        key_frame(0.5, 50),
        key_frame(1, 50)
    };
    
    float opacity = this->seekValueOfProgress(opacities, 4, progress);
    _sky->setOpacity(opacity);
}

float Eva4ExplodeScene::seekValueOfProgress(key_frame* keyFrames, int framesCount, float progress){
    float value;
    static int _keyframeIndex = 0;
    while (1) {
        key_frame present = keyFrames[_keyframeIndex];
        float pProgress = present.progress;
        if (pProgress < progress) {
            key_frame next = keyFrames[_keyframeIndex + 1];
            if (next.progress > progress) {
                float scale = (progress - present.progress) / (next.progress - present.progress);
                value = scale * (next.value - present.value) + present.value;
                break;
            } else {
                _keyframeIndex++;
            }
        }else if (pProgress == progress){
            value = keyFrames[_keyframeIndex].value;
            break;
        }else if (pProgress > progress){
            _keyframeIndex--;
        }
    }
    return value;
}

