#define kExplodeEnergyPlistName "ExplodeEnergy.plist"

#ifndef __Eva4Explode__Eva4ParticelSystemEnergy__
#define __Eva4Explode__Eva4ParticelSystemEnergy__
#include "cocos2d.h"
#include "CDParticleSystemProgress.h"

class Eva4ParticleSystemEnergy : public CDParticleSystemProgress {
public:
    virtual bool initWithDuration(float duration);
private:
    virtual void updateParticle(tCCParticle* particle, float progress);
};

#endif /* defined(__Eva4Explode__Eva4ParticelSystemEnergy__) */
