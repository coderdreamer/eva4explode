#ifndef __Eva4Explode__Eva4ParticleSystemCloud__
#define __Eva4Explode__Eva4ParticleSystemCloud__

#define kExplodeCloudPlistName "ExplodeCloud.plist"

#include "cocos2d.h"
#include "CDParticleSystemProgress.h"
using namespace cocos2d;

class Eva4ParticleSystemCloud: public CDParticleSystemProgress {
public:
    virtual bool initWithDuration(float duration);
    virtual void timeIncrease(float increament);
    virtual void play();
    virtual void pause();
private:
    virtual void updateParticle(tCCParticle* particle, float progress);
};

#endif /* defined(__Eva4Explode__Eva4ParticleSystemCloud__) */
