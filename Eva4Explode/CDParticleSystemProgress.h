#ifndef __Eva4Explode__CDParticleSystemProgress__
#define __Eva4Explode__CDParticleSystemProgress__

#include "cocos2d.h"
using namespace cocos2d;

struct key_frame {
    float progress;
    float value;
    key_frame(float progress, float value):progress(progress), value(value){};
};

class CDParticleSystemProgress : public CCParticleSystemQuad{
protected:
    float _duration;
    float _playbackTime;
    float _progress;
    bool _updateProgress;
    int _keyframeIndex;
    virtual void update(float dt);
    virtual void updateQuadWithParticle(tCCParticle* particle, const CCPoint& newPosition);
    virtual void updateParticle(tCCParticle* particle, float progress){};
public:
    virtual bool initWithFile(const char *plistFile);
    bool initWithFileAndDuration(const char *plistFile, float duration);
    CREATE_FUNC(CDParticleSystemProgress);
    void setProgress(float progress);
    float getProgress();
    void setUpdateProgress(bool update);
    virtual void timeIncrease(float increament);
    virtual void play();
    virtual void pause();
private:
};

float seekValueOfProgress(float progress, int* index, key_frame* keyFrames, int framesCount);

#endif /* defined(__Eva4Explode__CDParticleSystemProgress__) */
