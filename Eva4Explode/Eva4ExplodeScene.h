#ifndef __Eva4Explode__Eva4ExplodeScene__
#define __Eva4Explode__Eva4ExplodeScene__

#include "cocos2d.h"
#include "Eva4Explode.h"
#include "CCHolder.h"

class Eva4ExplodeScene : public cocos2d::CCScene, public CCHolderDelegate, public Eva4ExplodeDelegate
{
public:
    virtual bool init();
    CREATE_FUNC(Eva4ExplodeScene);
private:
    CCSprite* _sky;
    CCHolder* _holder;
    Eva4Explode* _explode;
    CCSprite* _ground;
    virtual void holderTouchedDown(CCHolder* holder);
    virtual void holderTouchedUp(CCHolder* holder);
    virtual void holderSingleTaped(CCHolder* holder);
    virtual void holderDoubleTaped(CCHolder* holder);
    virtual void holderStartHovering(CCHolder* holder);
    virtual void holderEndHovering(CCHolder* holder);
    virtual void holderMovingHorizontally(CCHolder* holder, float increament);
    virtual void explodeUpdateProgress(Eva4Explode *explode, float progress);
    
    float seekValueOfProgress(key_frame* keyFrames, int framesCount, float progress);
};

#endif // __HELLOWORLD_SCENE_H__

