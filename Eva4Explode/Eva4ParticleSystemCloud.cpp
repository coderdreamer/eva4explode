#include "Eva4ParticleSystemCloud.h"

bool Eva4ParticleSystemCloud::initWithDuration(float duration){
    if (!CDParticleSystemProgress::initWithFileAndDuration(kExplodeCloudPlistName, duration)) return false;
    this->pause();
    return true;
}

void Eva4ParticleSystemCloud::updateParticle(tCCParticle *particle, float progress){
    particle->timeToLive = 1;
    static float width = CCDirector::sharedDirector()->getWinSize().width;
    
    static key_frame radiuses[4] = {
        key_frame(0, width * 0),
        key_frame(0.1, width * 0),
        key_frame(0.3, width * .3),
        key_frame(1, width * .4)
    };
    static int radiusIndex = 0;
    float radius = seekValueOfProgress(_progress, &radiusIndex, radiuses, 4);
    particle->modeB.radius = radius;
    
    static key_frame sizes[4] = {
        key_frame(0, width * 0),
        key_frame(0.1, width * 0),
        key_frame(0.2, width * .3),
        key_frame(1, width * .12)
    };
    static int sizeIndex = 0;
    float size = seekValueOfProgress(_progress, &sizeIndex, sizes, 4);
    particle->size = size;
}

void Eva4ParticleSystemCloud::timeIncrease(float increament){
    float newProgress = _progress + increament / _duration;
    this->setProgress(newProgress);
    if (!_updateProgress) this->update(increament);
}

void Eva4ParticleSystemCloud::play(){
    this->setUpdateProgress(true);
    this->scheduleUpdate();
}

void Eva4ParticleSystemCloud::pause(){
    this->setUpdateProgress(false);
    this->unscheduleUpdate();
}
