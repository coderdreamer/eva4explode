#include "Eva4ParticleSystemCore.h"

bool Eva4ParticleSystemCore::initWithDuration(float duration){
    if (!CDParticleSystemProgress::initWithFileAndDuration(kExplodeCorePlistName, duration)) return false;
    this->pause();
    return true;
}

void Eva4ParticleSystemCore::updateParticle(tCCParticle *particle, float progress){
    static float width = CCDirector::sharedDirector()->getWinSize().width;
    static key_frame sizes[4] = {
        key_frame(0, width * 0),
        key_frame(.1, width * .2),
        key_frame(.2, width * .3),
        key_frame(1, width * .95)
    };
    static int sizeIndex = 0;
    float size = seekValueOfProgress(_progress, &sizeIndex, sizes, 4);
    this->setEndSize(size);
    this->setStartSize(size);
}

